package main

import (
	"log"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/ramencatz/projects/arpg/modules/dynamodb"
	"gitlab.com/ramencatz/projects/arpg/modules/loot"
	"gitlab.com/ramencatz/projects/arpg/modules/lootweb"
)

var tableBasics *dynamodb.TableBasics
var tableBasicsError error

func init() {

	tableName := os.Getenv("TABLE")
	tableGsi := os.Getenv("TABLE_GSI")

	tableBasics, tableBasicsError = dynamodb.CreateTableBasics(tableName, tableGsi)

}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	log.Println(request)

	if tableBasicsError != nil {
		return events.APIGatewayProxyResponse{Body: "Internal Error", StatusCode: 500}, tableBasicsError
	}
	table, err := loot.CreateMasterProbabilityTable(tableBasics)

	if err != nil {
		log.Println(err)
	}
	(*table).SetItemCount(5)
	results := (*table).GenerateDrop()

	//make sure to separate the items in the list
	headers := map[string]string{"Content-Type": "text/html"}
	stringResult := lootweb.ExampleResult("Items based on Probability Sub tables", results)

	return events.APIGatewayProxyResponse{Body: stringResult, Headers: headers, StatusCode: 200}, nil

}

func main() {
	lambda.Start(handler)
}
