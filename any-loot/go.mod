require (
	github.com/aws/aws-lambda-go v1.36.1
	gitlab.com/ramencatz/golang/lootengine v0.0.0-20230101220537-a8c76c1bd34c // indirect
	gitlab.com/ramencatz/projects/arpg/modules/dynamodb v0.0.0-20221230171611-a0fd9b12d2c4
	gitlab.com/ramencatz/projects/arpg/modules/loot v0.0.0-20230101220420-7ee6ce217fd8
	gitlab.com/ramencatz/projects/arpg/modules/lootweb v0.0.0-20230101230556-ac0a2520012d
)

replace gopkg.in/yaml.v2 => gopkg.in/yaml.v2 v2.2.8

module any-loot

go 1.16
