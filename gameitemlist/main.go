package main

import (
	"log"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/ramencatz/projects/arpg/modules/dynamodb"
	"gitlab.com/ramencatz/projects/arpg/modules/lootweb"
)

var tableBasics *dynamodb.TableBasics
var tableBasicsError error

func init() {

	tableName := os.Getenv("TABLE")

	tableBasics, tableBasicsError = dynamodb.CreateTableBasics(tableName)

}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	log.Println(request)

	if tableBasicsError != nil {
		return events.APIGatewayProxyResponse{Body: "Internal Error", StatusCode: 500}, tableBasicsError
	}
	itemList, err := dynamodb.AllTheLootItems(tableBasics)

	if err != nil {
		log.Println(err)
	}

	//make sure to separate the items in the list
	headers := map[string]string{"Content-Type": "text/html"}
	stringResult := lootweb.ExampleTableData("Loot items in the game_items table", itemList)

	return events.APIGatewayProxyResponse{Body: stringResult, Headers: headers, StatusCode: 200}, nil

}

func main() {
	lambda.Start(handler)
}
